#!/usr/bin/env bash
DJANGO_SETTINGS_MODULE='api.settings.development' celery worker -A api -Q hello_queue --concurrency=4 --loglevel=debug