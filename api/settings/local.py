# -*- coding: utf-8 -*-

from .base import *

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
URL_API = 'http://127.0.0.1:8000'
DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.mysql',
        'NAME': 'cmn_bds',
        'USER': 'root',
        'PASSWORD': 'mysql',
        'HOST': '127.0.0.1',
        'PORT': '3306',
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'; SET foreign_key_checks = 0;",
        },
    }
}
