# -*- coding: utf-8 -*-

from .base import *

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
DEBUG = False
URL_API = 'http://127.0.0.1:8000'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'api_core',
        'USER': 'root',
        'PASSWORD': 'mysql',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    }
}