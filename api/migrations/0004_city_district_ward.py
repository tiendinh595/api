# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-04-13 04:07
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_investor'),
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('location', models.CharField(max_length=255)),
                ('type', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'tbl_city',
            },
        ),
        migrations.CreateModel(
            name='District',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('location', models.CharField(max_length=255)),
                ('type', models.CharField(max_length=255)),
                ('city', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='api.City')),
            ],
            options={
                'db_table': 'tbl_district',
            },
        ),
        migrations.CreateModel(
            name='Ward',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('location', models.CharField(max_length=255)),
                ('type', models.CharField(max_length=255)),
                ('district', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='api.District')),
            ],
            options={
                'db_table': 'tbl_ward',
            },
        ),
    ]
