# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-04-14 03:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0016_auto_20180414_0353'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='broker_type',
            field=models.IntegerField(default=1, help_text=b'1:personal, 2:enterprise', max_length=1, verbose_name=b'1:personal, 2:enterprise'),
        ),
    ]
