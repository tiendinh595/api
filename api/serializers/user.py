# -*- coding: utf-8 -*-

from rest_framework import serializers
from api.models.user import User
from .location import CitySerializer, DistrictSerializer, WardSerializer
from django.conf import settings


class UserSerializer(serializers.ModelSerializer):
    token = serializers.SerializerMethodField()
    city = CitySerializer()
    district = CitySerializer()
    ward = CitySerializer()
    avatar = serializers.SerializerMethodField()


    def get_token(self, user):
        return user.accesstoken.first().token

    def get_avatar(self, user):
        if user.avatar:
            return '{}/{}'.format(settings.URL_API, user.avatar)
        return '{}/{}'.format(settings.URL_API, 'storage/images/avatar.png')

    class Meta:
        model = User
        fields = (
        'id', 'name', 'username', 'token', 'birthday', 'gender', 'mobile', 'email', 'facebook', 'avatar', 'address',
        'ward', 'district', 'city', 'avatar')

class UserProductSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField()
    def get_avatar(self, user):
        if user.avatar:
            return '{}/{}'.format(settings.URL_API, user.avatar)
        return '{}/{}'.format(settings.URL_API, 'storage/images/avatar.png')
    class Meta:
        model = User
        fields = ('id', 'name', 'username', 'avatar', 'email')
