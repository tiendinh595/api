# -*- coding: utf-8 -*-
from rest_framework import serializers
from api.models import City, District, Ward, Product, ProductType
from api.serializers.user import UserProductSerializer
from api.serializers.location import WardSerializer, DistrictSerializer, CitySerializer
from api.serializers.file import FileSerializer
import json

import locale


def number_format(num, places=0):
    return locale.format("%.*f", (places, num), True)


class ProductTypeItemSerializer(serializers.ModelSerializer):
    childs = serializers.SerializerMethodField()

    def get_childs(self, product_type):
        return ProductTypeItemSerializer(product_type.get_all_children(), many=True).data

    class Meta:
        model = ProductType
        fields = ('id', 'name', 'master_id', 'childs', 'slug')


class ProductTypeDetailSerializer(serializers.ModelSerializer):
    childs = serializers.SerializerMethodField()
    master = ProductTypeItemSerializer()

    def get_childs(self, product_type):
        return ProductTypeItemSerializer(product_type.get_all_children(), many=True).data

    class Meta:
        model = ProductType
        fields = ('id', 'name', 'master', 'keyword', 'description', 'childs', 'master', 'slug')


class ProductDetailSerializer(serializers.ModelSerializer):
    user = UserProductSerializer()
    ward = WardSerializer()
    district = DistrictSerializer()
    city = CitySerializer()
    product_type_master = ProductTypeItemSerializer()
    product_type_child = ProductTypeItemSerializer()
    images = FileSerializer(many=True)
    lat = serializers.SerializerMethodField()
    lng = serializers.SerializerMethodField()
    price_from_format = serializers.SerializerMethodField()
    price_to_format = serializers.SerializerMethodField()

    def get_lat(self, product):
        return product.location.x

    def get_lng(self, product):
        return product.location.y

    def get_price_from_format(self, product):
        return number_format(product.price_from)

    def get_price_to_format(self, product):
        return number_format(product.price_to)

    class Meta:
        model = Product
        fields = ('id', 'user', 'title', 'description', 'product_type_master', 'product_type_child', 'address', 'ward',
                  'district', 'city', 'project', 'location', 'price_from', 'price_to', 'price_from_format',
                  'price_to_format', 'price_type', 'area', 'images',
                  'frontage_area', 'land_with', 'home_direction', 'balcony_direction', 'floor_number', 'room_number',
                  'toilet_number', 'interior', 'contact_name', 'contact_phone', 'contact_address', 'lat', 'lng',
                  'created_at',
                  'updated_at', 'slug', 'city_id', 'district_id', 'ward_id', 'product_type_child_id')


class ProductItemSerializer(serializers.ModelSerializer):
    ward = WardSerializer()
    district = DistrictSerializer()
    city = CitySerializer()
    images = FileSerializer(many=True)
    thumbnail = FileSerializer()
    lat = serializers.SerializerMethodField()
    lng = serializers.SerializerMethodField()
    price_from_format = serializers.SerializerMethodField()
    price_to_format = serializers.SerializerMethodField()

    def get_lat(self, product):
        return product.location.y

    def get_lng(self, product):
        return product.location.x

    def get_price_from_format(self, product):
        return number_format(product.price_from)

    def get_price_to_format(self, product):
        return number_format(product.price_to)

    def get_images(self, product):
        try:
            return json.loads(product.images)
        except:
            return []

    class Meta:
        model = Product
        fields = ('id', 'title', 'address', 'ward', 'district', 'city', 'project', 'location', 'price_from', 'price_to'
                  , 'price_from_format', 'price_to_format', 'price_type', 'area', 'images', 'thumbnail', 'floor_number',
                  'room_number', 'lat', 'lng', 'slug')
