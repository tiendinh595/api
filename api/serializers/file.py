# -*- coding: utf-8 -*-
from rest_framework import serializers
from api.models import File
from django.conf import settings

class FileSerializer(serializers.ModelSerializer):
    full_url = serializers.SerializerMethodField()

    def get_full_url(self, file):
        return '{}/{}'.format(settings.URL_API, file.url)

    class Meta:
        model = File
        fields = ('id', 'url', 'full_url')