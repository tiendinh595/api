# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from api.services.common import response
from api.models import City, District, Ward
from api.serializers.location import CitySerializer, DistrictSerializer, WardSerializer
from api.services.validate import Validator, ValidatorException


class LocationCity(APIView):
    #
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (TokenIsAuthenticated,)

    def get(self, request):
        """
        get cities
        ---

        response_serializer: api.serializers.location.CitySerializer

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        try:
            cities = City.objects.all()
            return response.success(CitySerializer(cities, many=True).data)
        except Exception as ex:
            return response.error(ex.__str__())


class LocationDistrict(APIView):
    #
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (TokenIsAuthenticated,)

    def get(self, request):
        """
        get districts
        ---

        response_serializer: api.serializers.location.DistrictSerializer

        parameters:
            - name: city_id
              description:
              required: true
              type: string
              paramType: query

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        try:
            Validator(request.GET, {
                "city_id": [
                    "required",
                    "exists:City,id"
                ]
            }, {
                "city_id": "Thành phố (Tỉnh)"
            }).fails(raise_exception=True)

            districts = District.objects.filter(city__id=request.GET.get('city_id')).all()
            return response.success(DistrictSerializer(districts, many=True).data)
        except ValidatorException as ex:
            return response.error(ex.message, ex.errors)
        except Exception as ex:
            return response.error(ex.__str__())


class LocationWard(APIView):
    #
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (TokenIsAuthenticated,)

    def get(self, request):
        """
        get wards
        ---

        response_serializer: api.serializers.location.WardSerializer

        parameters:
            - name: district_id
              description:
              required: true
              type: string
              paramType: query

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        try:
            Validator(request.GET, {
                "district_id": [
                    "required",
                    "exists:District,id"
                ]
            }, {
                "district_id": "Quận(Huyện)"
            }).fails(raise_exception=True)

            wards = Ward.objects.filter(district__id=request.GET.get('district_id')).all()
            return response.success(WardSerializer(wards, many=True).data)
        except ValidatorException as ex:
            return response.error(ex.message, ex.errors)
        except Exception as ex:
            return response.error(ex.__str__())