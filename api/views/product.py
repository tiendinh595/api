# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from api.authentication import TokenAuthentication
from api.permissions import TokenIsAuthenticated, IsAuthenticatedOrReadOnly
from api.services.common import response
from api.models import Product, User, ProductType
from django.contrib.gis.geos import Point
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from rest_framework.exceptions import ParseError
import json
import re
from django.db.models import Q
from api.models.file import File
# serializer
from api.serializers.product import ProductDetailSerializer, ProductItemSerializer, ProductTypeItemSerializer, ProductTypeDetailSerializer

# service
from api.services.validate import Validator, ValidatorException
from api.services.common.request import get_data_except
from api.services.common import file


class ProductTypeDetailView(APIView):

    def get(self, request, pk):
        """
        get detail

        ---
        response_serializer: api.serializers.product.ProductTypeDetailSerializer

        parameters_strategy: replace
        parameters:
           - name: pk
             description:
             required: true
             type: string
             paramType: path

        responseMessages:
           - code: 200
             message: Success Json Object
           - code: 400
             message: '{"error" : 400 , "message" : "Not Valid"}'
           - code: 403
             message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
           - code: 404
             message: '{"error" : 404 , "message" : "Not Found"}'
           - code: 500
             message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
           - application/json

        produces:
           - application/json
        """
        try:
            product_type = ProductType.objects.get(pk=pk)
            serializer = ProductTypeDetailSerializer(product_type)
            return response.success(serializer.data)
        except Exception as ex:
            return response.error(ex.__str__())

class ProductTypeView(APIView):
    def get(self, request):
        """
        get all product types

        ---
        response_serializer: api.serializers.product.ProductTypeItemSerializer

        parameters_strategy: replace
        parameters:
           - name: master_id
             description: default 0
             required: false
             type: string
             paramType: query

        responseMessages:
           - code: 200
             message: Success Json Object
           - code: 400
             message: '{"error" : 400 , "message" : "Not Valid"}'
           - code: 403
             message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
           - code: 404
             message: '{"error" : 404 , "message" : "Not Found"}'
           - code: 500
             message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
           - application/json

        produces:
           - application/json
        """
        try:
            master_id = request.GET.get('master_id', 0)
            product_types = ProductType.objects.filter(master_id=master_id)
            serializer = ProductTypeItemSerializer(product_types, many=True)
            return response.success(serializer.data)
        except Exception as ex:
            return response.error(ex.__str__())


class ProductDetailView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, pk=None):
        """
        get detail product

        ---
        response_serializer: api.serializers.product.ProductDetailSerializer

        parameters_strategy: replace
        parameters:
            - name: pk
              description:
              required: true
              type: string
              paramType: path
            - name: is_edit
              description:
              required: false
              type: string
              paramType: query

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        try:
            is_edit = request.GET.get('is_edit')
            product = Product.objects
            if is_edit:
                product = product.filter(user_id=request.auth)
            product = product.filter(id=pk).first()
            print request.user
            if not product:
                raise Product.DoesNotExist()

            serializer = ProductDetailSerializer(product)
            return response.success(serializer.data)
        except Product.DoesNotExist:
            return response.error('Bài đăng không tồn tại')
        except Exception as ex:
            return response.error(ex.__str__())


class ProductView(APIView):
    #
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request):
        """
        get products

        ---

        response_serializer: api.serializers.product.ProductItemSerializer

        parameters:
            - name: city_id
              description:
              required: false
              type: string
              paramType: query
            - name: district_id
              description:
              required: false
              type: string
              paramType: query
            - name: ward_id
              description:
              required: false
              type: string
              paramType: query
            - name: product_type_master_id
              description:
              required: false
              type: string
              paramType: query
            - name: product_type_child_id
              description:
              required: false
              type: string
              paramType: query
            - name: price_from
              description:
              required: false
              type: string
              paramType: query
            - name: price_to
              description:
              required: false
              type: string
              paramType: query
            - name: area_from
              description:
              required: false
              type: string
              paramType: query
            - name: area_to
              description:
              required: false
              type: string
              paramType: query
            - name: title
              description:
              required: false
              type: string
              paramType: query
            - name: location
              description: lng,lat
              required: false
              type: string
              paramType: query
            - name: page
              description: page > 0 ( default is 1 )
              required: false
              type: string
              paramType: query
            - name: limit
              description: limit  ( default is 2 )
              required: false
              type: string
              paramType: query

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        try:
            data = request.GET

            Validator(data, {
                "city_id": ["nullable", "num"],
                "district_id": ["nullable", "num"],
                "ward_id": ["nullable", "num"],
                "product_type_master_id": ["nullable", "num"],
                "product_type_child_id": ["nullable", "num"],
                "price_from": ["nullable", "num"],
                "price_to": ["nullable", "num"],
                "area_from": ["nullable", "num"],
                "area_to": ["nullable", "num"],

            }, {
                          "city_id": "Tỉnh(Thành phố)",
                          "district_id": "Quận(Huyện)",
                          "ward_id": "Phường(xã)",
                          "product_type_master_id": "loại bất động sản",
                          "product_type_child_id": "loại bất động sản",
                          "area_from": "Diện tích",
                          "area_to": "Diện tích",
                          "price_from": "Giá",
                          "price_to": "Giá",
                      }).fails(raise_exception=True)

            page = data.get('page', '1')
            limit = data.get('limit', '20')

            try:
                page, limit = int(page), int(limit)
                if page <= 0:
                    page = 1
            except:
                raise ParseError("Page is not integer")

            products = Product.objects

            if data.get('city_id'):
                products = products.filter(city_id=data.get('city_id'))
            if data.get('district_id'):
                products = products.filter(district_id=data.get('district_id'))
            if data.get('ward_id'):
                products = products.filter(ward_id=data.get('ward_id'))
            if data.get('product_type_master_id'):
                products = products.filter(product_type_master_id=data.get('product_type_master_id'))
            if data.get('product_type_child_id'):
                products = products.filter(product_type_child_id=data.get('product_type_child_id'))
            if data.get('price_from'):
                products = products.filter(price_from__gte=data.get('price_from'))
            if data.get('price_to'):
                products = products.filter(price_to__lte=data.get('price_to'))
            if data.get('area_from'):
                products = products.filter(area__gte=data.get('area_from'))
            if data.get('area_to'):
                products = products.filter(area__lte=data.get('area_to'))
            if data.get('title'):
                products = products.filter(title__contains=data.get('title'))

            products = products.order_by('-id')
            paginator = Paginator(products, limit)

            try:
                products = paginator.page(page)
                serializer = ProductItemSerializer(products, many=True)
            except PageNotAnInteger:
                raise ParseError("Page is not integer")
            except EmptyPage:
                serializer = ProductItemSerializer([], many=True)

            next_link = None
            if page < paginator.num_pages:
                if data.urlencode():
                    querystring = re.sub(r'\??&?page=\d+', '', data.urlencode())
                    next_link = '/products/?{}&page={}'.format(querystring, page+1)
                else:
                    next_link = '/products/?page={}'.format(page+1)

            meta_data = {
                "total_files": paginator.count,
                "total_pages": paginator.num_pages,
                "current_page": page,
                "next_link": next_link
            }
            return response.success(serializer.data, meta_data)

        except ValidatorException as ex:
            return response.error(ex.message, ex.errors)
        except Exception as ex:
            return response.error(ex.__str__())

    def post(self, request):
        """
        add new product

        ---

        response_serializer: api.serializers.product.ProductDetailSerializer

        parameters:
            - name: Authorization
              description: access token
              required: true
              type: string
              paramType: header
            - name: city_id
              description:
              required: true
              type: string
              paramType: form
            - name: district_id
              description:
              required: true
              type: string
              paramType: form
            - name: ward_id
              description:
              required: true
              type: string
              paramType: form
            - name: address
              description:
              required: true
              type: string
              paramType: form
            - name: product_type_master_id
              description:
              required: true
              type: string
              paramType: form
            - name: product_type_child_id
              description:
              required: true
              type: string
              paramType: form
            - name: price_from
              description:
              required: true
              type: string
              paramType: form
            - name: price_to
              description:
              required: true
              type: string
              paramType: form
            - name: area
              description:
              required: true
              type: string
              paramType: form
            - name: title
              description:
              required: true
              type: string
              paramType: form
            - name: description
              description:
              required: true
              type: string
              paramType: form
            - name: images
              description: array id File [1,2]
              required: true
              type: string
              paramType: form
            - name: frontage_area
              description:
              required: false
              type: string
              paramType: form
            - name: land_with
              description:
              required: false
              type: string
              paramType: form
            - name: home_direction
              description:
              required: false
              type: string
              paramType: form
            - name: balcony_direction
              description:
              required: false
              type: string
              paramType: form
            - name: floor_number
              description:
              required: false
              type: string
              paramType: form
            - name: room_number
              description:
              required: false
              type: string
              paramType: form
            - name: toilet_number
              description:
              required: false
              type: string
              paramType: form
            - name: interior
              description:
              required: false
              type: string
              paramType: form
            - name: location
              description: lng,lat
              required: true
              type: string
              paramType: form
            - name: contact_name
              description:
              required: true
              type: string
              paramType: form
            - name: contact_phone
              description:
              required: true
              type: string
              paramType: form
            - name: contact_address
              description:
              required: true
              type: string
              paramType: form


        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        try:
            validator = Validator(request.POST, {
                "city_id": ["required", "exists:City,id"],
                "district_id": ["required", "exists:District,id,city_id"],
                "ward_id": ["required", "exists:Ward,id,district_id"],
                "address": ["required"],
                "product_type_master_id": ["required", "exists:ProductType,id"],
                "product_type_child_id": ["required", "exists:ProductType,id"],
                "price_from": ["required", "num"],
                "price_to": ["required", "num"],
                "area": ["required", "num"],
                "title": ["required", "max:255"],
                "description": ["required"],
                "images": ["required", "json"],
                "frontage_area": ["nullable", "num"],
                "land_with": ["nullable", "num"],
                "home_direction": ["nullable", "in:0,1,2,3,4,5,6,7,8"],
                "balcony_direction": ["nullable", "in:0,1,2,3,4,5,6,7,8"],
                "floor_number": ["nullable", "int"],
                "room_number": ["nullable", "int"],
                "toilet_number": ["nullable", "int"],
                "interior": ["nullable", "max:2000"],
                "location": ["required"],
                "contact_name": ["required"],
                "contact_phone": ["required"],
                "contact_address": ["required"],
                "captcha": [
                    "required",
                    'captcha'
                ]

            }, {
                          "city_id": "Tỉnh(Thành phố)",
                          "district_id": "Quận(Huyện)",
                          "ward_id": "Phường(xã)",
                          "address": "Địa chỉ",
                          "product_type_master_id": "loại bất động sản",
                          "product_type_child_id": "loại bất động sản",
                          "area": "Diện tích",
                          "title": "Tiêu đề",
                          "description": "Mô tả",
                          "images": "Hình ảnh",
                          "frontage_area": "Mặt tiền",
                          "land_with": "Hẻm",
                          "home_direction": "Hướng nhà",
                          "balcony_direction": "Hướng ban công",
                          "floor_number": "Số lầu",
                          "room_number": "Số phòng",
                          "toilet_number": "Số toilet",
                          "interior": "Nội thất",
                          "location": "Vị trí",
                          "contact_name": "Người liên hệ",
                          "contact_phone": "Số di động",
                          "contact_address": "Địa chỉ",
                      })

            validator.fails(raise_exception=True)

            data  = validator.get_data_clear(['images', 'captcha'])
            data['user'] = User.objects.get(id=request.user)
            data['location'] = data['location'].split(',')
            data['location'] = Point(float(data['location'][0]), float(data['location'][1]))
            data['slug'] = data['title']
            print data
            product = Product.objects.create(**data)

            if request.POST.get('images'):
                try:
                    product.images.clear()
                    images = json.loads(request.POST.get('images'))
                    product.images.add(*images)

                    # generate tbumbnail
                    image = File.objects.get(pk=images[0])
                    thumbnail_url, result_generate = file.generate_thumbnail(image.url)
                    if thumbnail_url != None:
                        thumbnail = File.objects.create(url=thumbnail_url, user_id=request.user)
                        product.thumbnail = thumbnail
                        product.save()
                except Exception as e:
                    print e
                    pass


            serializer = ProductDetailSerializer(product)
            return response.success(serializer.data,{}, "Đăng bài thành công")
        except ValidatorException as ex:
            return response.error(ex.message, ex.errors)
        except Exception as ex:
            print ex
            return response.error(ex.__str__())

    def put(self, request):
        """
        update product

        ---

        response_serializer: api.serializers.product.ProductDetailSerializer

        parameters:
            - name: Authorization
              description: access token
              required: true
              type: string
              paramType: header
            - name: product_id
              description:
              required: true
              type: string
              paramType: form
            - name: city_id
              description:
              required: false
              type: string
              paramType: form
            - name: district_id
              description:
              required: false
              type: string
              paramType: form
            - name: ward_id
              description:
              required: false
              type: string
              paramType: form
            - name: address
              description:
              required: false
              type: string
              paramType: form
            - name: product_type_master_id
              description:
              required: false
              type: string
              paramType: form
            - name: product_type_child_id
              description:
              required: false
              type: string
              paramType: form
            - name: price_from
              description:
              required: false
              type: string
              paramType: form
            - name: price_to
              description:
              required: false
              type: string
              paramType: form
            - name: area
              description:
              required: false
              type: string
              paramType: form
            - name: title
              description:
              required: false
              type: string
              paramType: form
            - name: description
              description:
              required: false
              type: string
              paramType: form
            - name: images
              description: array id File [1,2]
              required: false
              type: string
              paramType: form
            - name: frontage_area
              description:
              required: false
              type: string
              paramType: form
            - name: land_with
              description:
              required: false
              type: string
              paramType: form
            - name: home_direction
              description:
              required: false
              type: string
              paramType: form
            - name: balcony_direction
              description:
              required: false
              type: string
              paramType: form
            - name: floor_number
              description:
              required: false
              type: string
              paramType: form
            - name: room_number
              description:
              required: false
              type: string
              paramType: form
            - name: toilet_number
              description:
              required: false
              type: string
              paramType: form
            - name: interior
              description:
              required: false
              type: string
              paramType: form
            - name: location
              description: lng,lat
              required: false
              type: string
              paramType: form
            - name: contact_name
              description:
              required: false
              type: string
              paramType: form
            - name: contact_phone
              description:
              required: false
              type: string
              paramType: form
            - name: contact_address
              description:
              required: false
              type: string
              paramType: form


        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        try:
            product = Product.objects.get(pk=request.POST.get('product_id'), user_id=request.auth)

            Validator(request.POST, {
                "city_id": ["nullable", "exists:City,id"],
                "district_id": ["nullable", "exists:District,id,city_id"],
                "ward_id": ["nullable", "exists:Ward,id,district_id"],
                "address": ["nullable"],
                "product_type_master_id": ["nullable", "exists:ProductType,id"],
                "product_type_child_id": ["nullable", "exists:ProductType,id"],
                "price_from": ["nullable", "num"],
                "price_to": ["nullable", "num"],
                "area": ["nullable", "num"],
                "title": ["nullable", "max:255"],
                "description": ["nullable"],
                "images": ["nullable", "json"],
                "frontage_area": ["nullable", "num"],
                "land_with": ["nullable", "num"],
                "home_direction": ["nullable", "in:0,1,2,3,4,5,6,7,8"],
                "balcony_direction": ["nullable", "in:0,1,2,3,4,5,6,7,8"],
                "floor_number": ["nullable", "int"],
                "room_number": ["nullable", "int"],
                "toilet_number": ["nullable", "int"],
                "interior": ["nullable", "max:2000"],
                "location": ["nullable"],
                "contact_name": ["nullable"],
                "contact_phone": ["nullable"],
                "contact_address": ["nullable"],
                "captcha": [
                    "required",
                    'captcha'
                ]

            }, {
              "city_id": "Tỉnh(Thành phố)",
              "district_id": "Quận(Huyện)",
              "ward_id": "Phường(xã)",
              "address": "Địa chỉ",
              "product_type_master_id": "loại bất động sản",
              "product_type_child_id": "loại bất động sản",
              "area": "Diện tích",
              "title": "Tiêu đề",
              "description": "Mô tả",
              "images": "Hình ảnh",
              "frontage_area": "Mặt tiền",
              "land_with": "Hẻm",
              "home_direction": "Hướng nhà",
              "balcony_direction": "Hướng ban công",
              "floor_number": "Số lầu",
              "room_number": "Số phòng",
              "toilet_number": "Số toilet",
              "interior": "Nội thất",
              "location": "Vị trí",
              "contact_name": "Người liên hệ",
              "contact_phone": "Số di động",
              "contact_address": "Địa chỉ",
          }).fails(raise_exception=True)

            data = {
                "city_id": request.POST.get('city_id', product.city_id),
                "district_id": request.POST.get('district_id', product.district_id),
                "ward_id": request.POST.get('ward_id', product.ward_id),
                "address": request.POST.get('address', product.address),
                "product_type_master_id": request.POST.get('product_type_master_id', product.product_type_master_id),
                "product_type_child_id": request.POST.get('product_type_child_id', product.product_type_child_id),
                "price_from": request.POST.get('price_from', product.price_from),
                "price_to": request.POST.get('price_from', product.price_to),
                "area": request.POST.get('area', product.area),
                "title": request.POST.get('title', product.title),
                "description": request.POST.get('description', product.description),
                "images": request.POST.get('images'),
                "frontage_area": request.POST.get('frontage_area', product.frontage_area),
                "land_with": request.POST.get('land_with', product.land_with),
                "home_direction": request.POST.get('home_direction', product.home_direction),
                "balcony_direction": request.POST.get('balcony_direction', product.balcony_direction),
                "floor_number": request.POST.get('floor_number', product.floor_number),
                "room_number": request.POST.get('room_number', product.room_number),
                "toilet_number": request.POST.get('toilet_number', product.toilet_number),
                "interior": request.POST.get('interior', product.interior),
                "location": request.POST.get('location'),
                "contact_name": request.POST.get('contact_name', product.contact_name),
                "contact_phone": request.POST.get('contact_phone', product.contact_phone),
                "contact_address": request.POST.get('contact_address', product.contact_address),
            }

            if data['floor_number'] == '':
                del data['floor_number']
            if data['room_number'] == '':
                del data['room_number']
            if data['toilet_number'] == '':
                del data['toilet_number']

            print data

            if data['location']:
                data['location'] = data['location'].split(',')
                data['location'] = Point(float(data['location'][0]), float(data['location'][1]))
            else:
                data['location'] = product.location

            if data['images']:
                try:
                    product.images.clear()
                    images = json.loads(request.POST.get('images'))
                    product.images.add(*images)
                except:
                    pass

            del data['images']

            Product.objects.filter(pk=request.POST.get('product_id')).update(**data)
            product = Product.objects.get(pk=request.POST.get('product_id'))

            serializer = ProductDetailSerializer(product)
            return response.success(serializer.data, {}, "Cập nhật thành công")
        except ValidatorException as ex:
            return response.error(ex.message, ex.errors)
        except Product.DoesNotExist:
            return response.error('Bài đăng không tồn tại')
        except Exception as ex:
            print ex
            return response.error(ex.__str__())

    def delete(self, request, *args, **kwargs):
        """
        delete product
        ---
        parameters_strategy: replace
        parameters:
            - name: Authorization
              description: access token
              required: true
              type: string
              paramType: header
            - name: pk
              description:
              required: true
              type: string
              paramType: query

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        try:
            import datetime
            pk = request.GET.get('pk')
            product = Product.objects.get(pk=pk, user_id=request.auth)
            product.deleted_at = datetime.datetime.now()
            product.save()
            return response.confirm("xóa bài viết thành công")
        except ValidatorException as ex:
            return response.error(ex.message, ex.errors)
        except Product.DoesNotExist:
            return response.error('Bài đăng không tồn tại')
        except Exception as ex:
            print ex
            return response.error(ex.__str__())



class ProductRelateView(APIView):
    def get(self, request, pk):
        """
        get all products relates

        ---
        response_serializer: api.serializers.product.ProductItemSerializer

        parameters_strategy: replace
        parameters:
           - name: pk
             description:
             required: true
             type: string
             paramType: path

        responseMessages:
           - code: 200
             message: Success Json Object
           - code: 400
             message: '{"error" : 400 , "message" : "Not Valid"}'
           - code: 403
             message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
           - code: 404
             message: '{"error" : 404 , "message" : "Not Found"}'
           - code: 500
             message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
           - application/json

        produces:
           - application/json
        """
        try:
            categories = Product.objects.values('product_type_child_id', 'product_type_master_id').get(pk=pk)
            categories = [v for k, v in  categories.items()]
            products = Product.objects.filter(Q(product_type_child_id__in=categories)|Q(product_type_master_id__in=categories)).order_by('?')[:4]
            serializer = ProductItemSerializer(products, many=True)
            return response.success(serializer.data)
        except Exception as ex:
            return response.error(ex.__str__())