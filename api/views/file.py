# -*- coding: utf-8 -*
import os
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from api.services.common import response
from api.authentication import TokenAuthentication
from api.permissions import TokenIsAuthenticated
from api.services.validate import Validator, ValidatorException
from api.models import File, User
from api.serializers.file import FileSerializer
from api.services.common.file import handle_uploaded_file

class FileView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request):
        """
        upload new file
        ---

        response_serializer: api.serializers.file.FileSerializer

        parameters:
            - name: Authorization
              description: access token
              required: true
              type: string
              paramType: header
            - name: file
              description:
              required: true
              type: file
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        try:
            file_obj = request.FILES['file']
            Validator(request.FILES, {
                'file': [
                    "required",
                    "image",
                    "size"
                ]
            }).fails(raise_exception=True)
            file_name, uploaded =  handle_uploaded_file(file_obj)

            if uploaded == False:
                return response.error("upload thất bại")
            file = File.objects.create(
                url=file_name,
                user=User.objects.get(pk=request.user)
            )
            serializer = FileSerializer(file)
            return response.success(serializer.data)
        except ValidatorException as ex:
            return response.error(ex.message, ex.errors)
        except Exception as ex:

            return response.error(ex.__str__())



class FileDeleteView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)
    parser_classes = (MultiPartParser, FormParser)

    def delete(self, request, pk):
        """
        delete file
        ---

        parameters:
            - name: Authorization
              description: access token
              required: true
              type: string
              paramType: header
            - name: pk
              description:
              required: true
              type: string
              paramType: path

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        try:
            file = File.objects.filter(pk=pk, user_id=request.user)
            if file:
                file.delete()
                return response.success(None, None, 'Xóa file thành công')
            return response.error("file không tồn tại")
        except ValidatorException as ex:
            return response.error(ex.message, ex.errors)
        except Exception as ex:
            return response.error(ex.__str__())