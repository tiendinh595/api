# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from api.models import District, City, Investor

class Project(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    images = models.TextField(blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    ward = models.ForeignKey('Ward', models.DO_NOTHING, blank=True, null=True)
    district = models.ForeignKey(District, models.DO_NOTHING, blank=True, null=True)
    city = models.ForeignKey(City, models.DO_NOTHING, blank=True, null=True)
    project_type = models.ForeignKey('ProjectType', models.DO_NOTHING, blank=True, null=True)
    area = models.FloatField(blank=True, null=True)
    block_number = models.IntegerField(blank=True, null=True)
    room_number = models.IntegerField(blank=True, null=True)
    location = models.TextField(blank=True, null=True)  # This field type is a guess.
    investor = models.ForeignKey(Investor, models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'tbl_project'


class ProjectType(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    keyword = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'tbl_project_type'
