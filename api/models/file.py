# -*- coding: utf-8 -*-
from django.db import models
from api.models import User
import os
from django.conf import settings

class FileQueryset(models.QuerySet):
    def delete(self):
        url = self.first().url
        full_path = os.path.join(settings.BASE_DIR, url)
        if os.path.isfile(full_path):
            os.remove(full_path)
        return self.first().delete()

class FileManager(models.Manager):

    def get_queryset(self):
        return FileQueryset(self.model)


class File(models.Model):

    id = models.AutoField(primary_key=True)
    url = models.URLField(null=False, blank=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = FileManager()

        # super(File, self).save(*args, **kwargs)

    class Meta:
        db_table = 'tbl_files'