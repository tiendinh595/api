# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.gis.db import models
from api.models import District, City, File
from django.utils.text import slugify

class ProductType(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    master = models.ForeignKey("ProductType", models.CASCADE, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    slug = models.SlugField(null=True, blank=True)
    keyword = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    def get_all_children(self):
        return ProductType.objects.filter(master=self)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)

        super(ProductType, self).save(*args, **kwargs)

    class Meta:
        db_table = 'tbl_product_type'


class Product(models.Model):

    KXD = 0
    DONG = 1
    TAY = 2
    NAM = 3
    BAC = 4
    TAY_BAC = 5
    DONG_BAC = 6
    TAY_NAM = 7
    DONG_NAM = 8
    HOME_DIRECTION = (
        (KXD, 'Không xác định'),
        (DONG, 'Đông'),
        (TAY, 'Tây'),
        (NAM, 'Nam'),
        (BAC, 'Bắc'),
        (TAY_BAC, 'Tây Bắc'),
        (DONG_BAC, 'Đông Bắc'),
        (TAY_NAM, 'Tây Nam'),
        (DONG_NAM, 'Đông nam'),
    )
    BALCONY_DIRECTION = (
        (KXD, 'Không xác định'),
        (DONG, 'Đông'),
        (TAY, 'Tây'),
        (NAM, 'Nam'),
        (BAC, 'Bắc'),
        (TAY_BAC, 'Tây Bắc'),
        (DONG_BAC, 'Đông Bắc'),
        (TAY_NAM, 'Tây Nam'),
        (DONG_NAM, 'Đông nam'),
    )

    user = models.ForeignKey('User', models.DO_NOTHING, blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    slug = models.SlugField(null=True, blank=True)
    description = models.TextField(blank=True, null=True)
    product_type_master = models.ForeignKey('ProductType', models.DO_NOTHING, blank=True, null=True,
                                            related_name='product_type_master')
    product_type_child = models.ForeignKey('ProductType', models.DO_NOTHING, blank=True, null=True,
                                           related_name='product_type_child')
    address = models.CharField(max_length=255, blank=True, null=True)
    ward = models.ForeignKey('Ward', models.DO_NOTHING, blank=True, null=True)
    district = models.ForeignKey(District, models.DO_NOTHING, blank=True, null=True)
    city = models.ForeignKey(City, models.DO_NOTHING, blank=True, null=True)
    project = models.ForeignKey('Project', models.DO_NOTHING, blank=True, null=True)
    location = models.PointField(blank=True, null=True)  # This field type is a guess.
    price_from = models.FloatField(blank=True, null=True)
    price_to = models.FloatField(blank=True, null=True)
    price_type = models.IntegerField(blank=True, null=True)
    area = models.FloatField(blank=True, null=True)
    thumbnail = models.ForeignKey(File, related_name="thumbnail_product", default='', null=True, blank=True, on_delete=models.CASCADE)
    images = models.ManyToManyField(File)
    frontage_area = models.IntegerField(blank=True, null=True)
    land_with = models.IntegerField(blank=True, null=True)
    home_direction = models.IntegerField(blank=True, null=True, choices=HOME_DIRECTION, default=KXD)
    balcony_direction = models.IntegerField(blank=True, null=True, choices=BALCONY_DIRECTION, default=KXD)
    floor_number = models.IntegerField(blank=True, null=True)
    room_number = models.IntegerField(blank=True, null=True)
    toilet_number = models.IntegerField(blank=True, null=True)
    interior = models.TextField(blank=True, null=True)
    contact_name = models.CharField(max_length=100, blank=True, null=True)
    contact_phone = models.CharField(max_length=30, blank=True, null=True)
    contact_address = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ["-id"]
        db_table = 'tbl_product'

    def get_balcony_direction_display(self):
        print dict(self.HOME_DIRECTION)[int(self.home_direction)]
        return dict(self.BALCONY_DIRECTION)[int(self.balcony_direction)]

    def get_home_direction_display(self):
        raise Exception('xxx')

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)

        super(Product, self).save(*args, **kwargs)