# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Utility(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    utility_type = models.ForeignKey('UtilityType', models.DO_NOTHING, blank=True, null=True)
    location = models.TextField(blank=True, null=True)  # This field type is a guess.
    address = models.CharField(max_length=255, blank=True, null=True)
    ward_id = models.IntegerField(blank=True, null=True)
    district_id = models.IntegerField(blank=True, null=True)
    city_id = models.IntegerField(blank=True, null=True)
    creaed_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'tbl_utility'


class UtilityType(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'tbl_utility_type'

