# -*- coding: utf-8 -*-
from django.db import models
from api.models.location import Ward, City, District

class User(models.Model):

    PERSONAL = 1
    ENTERPRISE = 2
    ACCOUNT_TYPE = (
        (PERSONAL, 'personal'),
        (ENTERPRISE, 'enterprise'),
    )

    MALE = 1
    FEMALE = 2
    GENDER = (
        (MALE, 'male'),
        (FEMALE, 'female'),
    )

    username = models.CharField(max_length=100, blank=True, null=True)
    password = models.CharField(max_length=255, blank=True, null=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    birthday = models.DateField(blank=True, null=True)
    gender = models.IntegerField(default=MALE, choices=GENDER)
    mobile = models.CharField(max_length=30, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    facebook = models.CharField(max_length=255, blank=True, null=True)
    avatar = models.CharField(max_length=255, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    ward = models.ForeignKey(Ward, blank=True, null=True, on_delete=models.CASCADE, default=None)
    district = models.ForeignKey(District, on_delete=models.CASCADE, null=True, blank=True)
    city = models.ForeignKey(City, on_delete=models.CASCADE, null=True, blank=True)
    account_type = models.IntegerField(default=PERSONAL, choices=ACCOUNT_TYPE, help_text="1:personal, 2:enterprise")
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'tbl_user'
        unique_together = ('username', )


class AccessToken(models.Model):
    id = models.AutoField(primary_key=True)
    token = models.CharField(max_length=500, db_index=True)
    key = models.IntegerField(default=0)
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE, related_name='accesstoken', related_query_name='accesstoken')
    issued_at = models.DateTimeField(blank=True, null=True)
    expiry_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'tbl_access_token'
