# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class City(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    location = models.CharField(max_length=255)
    type = models.CharField(max_length=255)

    class Meta:
        db_table = 'tbl_city'


class District(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    location = models.CharField(max_length=255)
    type = models.CharField(max_length=255)
    city = models.ForeignKey(City, models.DO_NOTHING)

    class Meta:
        db_table = 'tbl_district'


class Ward(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    location = models.CharField(max_length=255)
    type = models.CharField(max_length=255)
    district = models.ForeignKey(District, models.DO_NOTHING)

    class Meta:
        db_table = 'tbl_ward'

