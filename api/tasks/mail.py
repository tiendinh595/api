# -*- coding: utf-8 -*-
from celery import shared_task, Celery
from django.core.mail import send_mail
from django.conf import settings

@shared_task
def send_mail_contact(email, content):
    send_mail(
        '[BDS CMN] Thư liên hệ ',
        content,
        settings.EMAIL_HOST_USER,
        [email],
        fail_silently=False,
    )