# -*- coding: utf-8 -*-
import sys
from django.http.request import QueryDict
from django.utils.functional import cached_property

import importlib


def class_for_name(module_name, class_name):
    m = importlib.import_module(module_name)
    c = getattr(m, class_name)
    return c


class ValidatorException(Exception):
    def __init__(self, message, errors):
        super(ValidatorException, self).__init__(message)
        self.message = 'dữ liệu không hợp lệ'
        self.errors = errors


class Validator(object):
    def __init__(self, data_validate, rules, name_fields={}):
        self.is_valid = True
        self.errors = {}
        self.data_validate = self.get_data_validate(data_validate)
        self.rules = rules
        self.name_fields = name_fields
        self.func_validator = {
            "required": self.validate_required,
            "str": self.validate_str,
            "int": self.validate_integer,
            "float": self.validate_float,
            "num": self.validate_num,
            "image": self.validate_image,
            "json": self.validate_json,
            "min": self.validate_min,
            "max": self.validate_max,
            "size": self.validate_size,
            "unique": self.validate_unique,
            "same": self.validate_same,
            "in": self.validate_in,
            "not_in": self.validate_not_in,
            "email": self.validate_email,
            "date": self.validate_date,
            "nullable": self.validate_nullable,
            "exists": self.validate_exists,
            "captcha": self.validate_recaptcha,
        }

    def check_rule_exists(self, field, rule):
        if field in self.rules:
            if rule in self.rules[field]:
                return True
        return False

    def get_data_clear(self, exclude = []):
        data_clear = {}
        for field in self.rules:
            if field not in exclude and field not in self.errors:
                value = self.get_value_of_field(field)
                if value != None:
                    data_clear[field] = value
        return data_clear

    def validate(self):

        for field in self.rules:
            rules = self.rules[field]
            for rule in rules:
                rule = str(rule).split(':')

                try:
                    rule_options = rule[1].split(',')
                except:
                    rule_options = []
                rule = rule[0]

                if rule not in self.func_validator:
                    raise Exception("rule '{}' is invalid".format(rule))
                else:
                    self.func_validator[rule](field, rule_options)

    def fails(self, raise_exception=False):
        self.validate()
        if raise_exception == True and not self.is_valid:
            raise ValidatorException("dữ liệu không hợp lệ", self.errors)
        return not self.is_valid

    def update_errors(self, field, error):

        # pass when field exist nullable
        if (self.get_value_of_field(field) == None and self.check_rule_exists(field, 'nullable')):
            return True


        self.is_valid = False
        if field in self.errors:
            self.errors[field] = [error] + self.errors[field]
        else:
            self.errors[field] = [error]

    def get_data_validate(self, data_validate):
        data = {}
        if isinstance(data_validate, QueryDict):
            for i in data_validate:
                data[i] = data_validate[i]
        else:
            data = data_validate
        return data

    def get_value_of_field(self, field):
        if field in self.data_validate:
            value = self.data_validate[field]
            if value == None or value == '' or len(value) ==  0 or value == 'null':
                return None
            return value
        return None

    def get_name_field(self, field):
        if field in self.name_fields:
            return self.name_fields[field]
        return field

    def validate_required(self, field, args):
        """required"""
        value = self.get_value_of_field(field)
        name = self.get_name_field(field)
        if not value or value == '' or len(value) == 0:
            self.update_errors(field, u"{} bắt buộc phải nhập".format(name))

    def validate_integer(self, field, args):
        """integer"""
        value = self.get_value_of_field(field)
        name = self.get_name_field(field)

        try:
            int(value)
        except:
            self.update_errors(field, u"{} phải là kiểu số nguyên".format(name))

    def validate_float(self, field, args):
        """float"""
        value = self.get_value_of_field(field)
        name = self.get_name_field(field)
        try:
            float(value)
        except:
            self.update_errors(field, u"{} phải là kiểu số thực".format(name))

    def validate_num(self, field, args):
        """num"""
        value = self.get_value_of_field(field)
        name = self.get_name_field(field)
        try:
            float(value)
        except:
            self.update_errors(field, u"{} phải là số".format(name))

    def validate_str(self, field, args):
        """str"""
        value = self.get_value_of_field(field)
        name = self.get_name_field(field)

        if not isinstance(value, str) and not isinstance(value, unicode):
            self.update_errors(field, u"{} phải là ký tự".format(name))

    def validate_min(self, field, args):
        """min:10000"""
        value = self.get_value_of_field(field)
        name = self.get_name_field(field)
        value_compare = int(args[0])

        if value == None:
            self.update_errors(field, u"{} tối thiểu {}".format(name, value_compare))
        if (isinstance(value, str) or isinstance(value, unicode)) and len(value) < value_compare:
            self.update_errors(field, u"{} tối thiểu {} ký tự".format(name, value_compare))
        elif isinstance(value, int) and value < value_compare:
            self.update_errors(field, u"{} tối thiểu {}".format(name, value_compare))

    def validate_max(self, field, args):
        """max:1000"""
        value = self.get_value_of_field(field)
        name = self.get_name_field(field)
        value_compare = int(args[0])

        if value == None:
            self.update_errors(field, u"{} tối đa {}".format(name, value_compare))
        if isinstance(value, str) and len(value) > value_compare:
            self.update_errors(field, u"{} tối đa {} ký tự".format(name, value_compare))
        elif isinstance(value, int) and value > value_compare:
            self.update_errors(field, u"{} tối đa {}".format(name, value_compare))

    def validate_unique(self, field, args):
        """unique:Model,column"""
        value = self.get_value_of_field(field)
        name = self.get_name_field(field)
        model = args[0]
        column = args[1]

        if value == None or len(value) == 0:
            return True

        model = class_for_name('api.models', model)

        if model.objects.values(column).filter(**{"{}".format(column): value}):
            self.update_errors(field, u"{} đã tồn tại".format(name))

    def validate_same(self, field, args):
        """same:field"""
        value = self.get_value_of_field(field)
        name = self.get_name_field(field)
        field_compare = args[0]

        if self.get_value_of_field(field_compare) != value:
            self.update_errors(field, u"{} không khớp".format(name))

    def validate_in(self, field, args):
        """in:db,model,column | in:1,2"""
        value = self.get_value_of_field(field)
        name = self.get_name_field(field)

        if (args[0] == 'db'):
            model = args[1]
            column = args[2]
            model = class_for_name('api.models', model)

            if value == None :
                return True

            if not model.objects.values(column).filter(**{"{}".format(column): value}):
                self.update_errors(field, u"{} không hợp lệ".format(name))
        elif str(value) not in args:
            self.update_errors(field, u"{} không hợp lệ".format(name))

    def validate_not_in(self, field, args):
        """not_in:db,model,column | not_in:1,2"""
        value = self.get_value_of_field(field)
        name = self.get_name_field(field)

        if (args[0] == 'db'):
            model = args[1]
            column = args[2]
            if value == None :
                return True
            model = class_for_name('api.models', model)
            if model.objects.values(column).filter(**{"{}".format(column): value}):
                self.update_errors(field, u"{} không hợp lệ".format(name))
        elif str(value) in args:
            self.update_errors(field, u"{} không hợp lệ".format(name))

    def validate_exists(self, field, args):
        """not_in:db,model,column | not_in:1,2"""
        value = self.get_value_of_field(field)
        name = self.get_name_field(field)

        model = args[0]
        column = args[1]

        if value == None:
            return True

        if(value == None or value == '' or len(value) ==  0):
            return self.update_errors(field, u"{} không hợp lệ".format(name))

        try:
            other_column = args[2]
            condition = {"{}".format(column): value, "{}".format(other_column): self.get_value_of_field(other_column)}
        except:
            condition = {"{}".format(column): value}

        model = class_for_name('api.models', model)

        try:
            if not model.objects.values(column).filter(**condition):
                self.update_errors(field, u"{} không hợp lệ".format(name))
        except:
            self.update_errors(field, u"{} không hợp lệ".format(name))

    def validate_email(self, field, args):
        """email"""
        from django.core.validators import validate_email
        from django.core.exceptions import ValidationError

        value = self.get_value_of_field(field)
        name = self.get_name_field(field)

        try:
            validate_email(value)
        except ValidationError:
            self.update_errors(field, u"{} không không hợp lệ".format(name))

    def validate_date(self, field, args):
        """date"""
        import datetime

        value = self.get_value_of_field(field)
        name = self.get_name_field(field)

        try:
            datetime.datetime.strptime(value, '%Y-%m-%d')
        except:
            self.update_errors(field, u"{} không phải ngày tháng".format(name))

    def validate_json(self, field, args):
        """json"""
        import json
        value = self.get_value_of_field(field)
        name = self.get_name_field(field)

        try:
            json_value = json.loads(value)
            if (self.check_rule_exists(field, 'required') and len(json_value) == 0):
                self.update_errors(field, u"{} bắt buộc phải nhập".format(name))

        except:
            self.update_errors(field, u"{} không phải là json".format(name))

    def validate_image(self, field, args):
        from django.conf import settings
        name = self.get_name_field(field)
        value = self.get_value_of_field(field)

        try:
            if value.content_type not in settings.ALLOWED_EXTENSIONS:
                self.update_errors(field, u"{} định dạng ảnh không hợp lệ".format(name))
        except Exception as e:
            self.update_errors(field, u"{} không phải là hình ảnh".format(name))

    def validate_size(self, field, args):
        from django.conf import settings
        name = self.get_name_field(field)
        value = self.get_value_of_field(field)

        try:
            max_size = args[0]
        except:
            max_size = settings.MAX_FILE_ZISE

        try:
            if value.size > max_size:
                self.update_errors(field, u"kích thước {} vượt quá giới hạn".format(name))
        except:
            self.update_errors(field, u"{} không phải là tệp tin".format(name))

    def validate_recaptcha(self, field, args):
        from django.conf import settings
        import urllib
        import urllib2
        import json

        name = self.get_name_field(field)
        value = self.get_value_of_field(field)

        url = 'https://www.google.com/recaptcha/api/siteverify'
        values = {
            'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
            'response': value
        }
        data = urllib.urlencode(values)
        req = urllib2.Request(url, data)
        response = urllib2.urlopen(req)
        result = json.load(response)

        if not result['success']:
            self.update_errors(field, u"{} không đúng".format(name))

    def validate_nullable(self, field, args):
        pass
