# -*- coding: utf-8 -*-
from django.http import JsonResponse
from rest_framework import status
from django.http import HttpResponse

def fail(message, errors = {}):
    # localize message here
    # custom data here
    return JsonResponse({"code":400,"message":message, "errors": errors}, safe=False, status=status.HTTP_200_OK)

def error(message = '',errors = {}):
    # localize message here
    # custom data here
    return JsonResponse({"code":500,"message":message, "errors": errors}, safe=False, status=status.HTTP_200_OK)

def unauthorize(message, errors = {}):
    # localize message here
    # custom data here
    return JsonResponse({"code":401,"message":message, "errors": errors}, safe=False, status=status.HTTP_200_OK)

def forbidden(message, errors = {}):
    # localize message here
    # custom data here
    return JsonResponse({"code":403,"message":message, "errors": errors}, safe=False, status=status.HTTP_200_OK)

def confirm(message):
    return JsonResponse({"code":0,"message":message}, safe=False, status=status.HTTP_200_OK)

def success(data = None, metadata = {}, message = ""):
    return JsonResponse({"code":200, "data": data, "metadata": metadata, "message":message}, safe=False, status=status.HTTP_200_OK)
