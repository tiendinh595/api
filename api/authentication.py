# -*- coding: utf-8 -*-
from rest_framework.authentication import BaseAuthentication
from rest_framework import exceptions
from api.models.user import AccessToken, User
import jwt
from django.conf import settings

class TokenAuthentication(BaseAuthentication):
    def authenticate(self, request):
        token = request.META.get('HTTP_AUTHORIZATION')
        try:
            if token:
                try:
                    options = {
                        'verify_exp': True,
                    }
                    token = jwt.decode(token, settings.JWT.get('secret'), options=options)
                    token = AccessToken.objects.get(pk=token.get('id'), key=token.get('key'))
                except:
                    raise exceptions.AuthenticationFailed('Token not found')
                return (token.user_id, token.user_id)

            else:
                raise exceptions.AuthenticationFailed('Token not found')
        except:
            return (None, None)
