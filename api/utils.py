# -*- coding: utf-8 -*-

from rest_framework.views import exception_handler
import hashlib


def custom_exception_handler(exc, context):
    """
    Custom exception handler for Django Rest Framework that adds
    the `status_code` to the response and renames the `detail` key to `error`.
    """
    response = exception_handler(exc, context)

    if response is not None:
        response.data['code'] = response.status_code
        response.data['message'] = response.data['detail']
        response.data['errors'] = {}
        del response.data['detail']

    return response

def md5(str):
    m = hashlib.md5()
    m.update(str)
    return m.hexdigest()