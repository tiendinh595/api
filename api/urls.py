from django.conf.urls import url, include
from api.views import user, location, product, file, mail
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    url(r'^docs/', include('rest_framework_swagger.urls')),
    url(r'^login', user.Login.as_view()),
    url(r'^register', user.Register.as_view()),
    url(r'^files/', include([
        url(r'(?P<pk>\d+)', file.FileDeleteView.as_view()),
        url(r'', file.FileView.as_view()),
    ])),
    url(r'^profile', user.Profile.as_view()),
    url(r'^mail', mail.MailView.as_view()),
    url(r'^products/', include([
        url(r'(?P<pk>\d+)/relate', product.ProductRelateView.as_view()),
        url(r'(?P<pk>\d+)', product.ProductDetailView.as_view()),
        url(r'', product.ProductView.as_view())
    ])),
    url(r'^product-types/', include([
        url(r'(?P<pk>\d+)', product.ProductTypeDetailView.as_view()),
        url(r'', product.ProductTypeView.as_view())
    ])),
    url(r'^location/', include([
        url(r'cities', location.LocationCity.as_view()),
        url(r'districts', location.LocationDistrict.as_view()),
        url(r'wards', location.LocationWard.as_view()),
    ]))
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.STORAGE_URL, document_root=settings.STORAGE_ROOT)
